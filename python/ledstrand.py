import socket
import struct
import time
import websocket

class LightColorFrame:
    """ Represents one frame to send to the LED light strand
    
    Data format:
    Header:
    0                   1
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5
    *-------------------------------*
    | Action        | Count of parts|
    *-------------------------------*
    
    Part:
    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2
    *---------------------------------------------------------------*
    | LED Index     | Red           | Green         | Blue          |
    *---------------------------------------------------------------*
    
    """
    def __init__(self):
        self.leds = {}

    def set_led(self, pos, r, g, b):
        self.leds[pos] = (r, g, b)

    def to_bytes(self):
        frame_data = list()

        # Attach header
        frame_header = struct.pack("cB", b"S", len(self.leds))
        frame_data.append(frame_header)

        # Add individual LEDs
        for pos, (r, g, b) in self.leds.items():
            segment = struct.pack("BBBB", pos, r, g, b)
            frame_data.append(segment)

        return b"".join(frame_data)

class LEDStrand:
    def __init__(self, hostname, port=8080):
        self.ws = websocket.WebSocket()
        self.ws.connect("ws://{}:{:d}/".format(hostname, port),
                        sockopt=((socket.IPPROTO_TCP, socket.TCP_NODELAY)))

    def send_frame(self, frame):
        self.ws.send_binary(frame.to_bytes())